<?php
require_once __dir__ . '/vendor/autoload.php';
$loader = new Nette\Loaders\RobotLoader;
$loader->addDirectory( __DIR__ . '/app' );
$loader->setTempDirectory( __DIR__ . '/temp' );
$loader->setAutoRefresh( true );
$loader->register();
$containerLoader = new Nette\DI\ContainerLoader( __dir__ . '/temp' );
if( !file_exists( __dir__ . '/app/config/config.local.neon' ) ) {
	file_put_contents( __dir__ . '/app/config/config.local.neon', '' );
}
$class = $containerLoader->load( function( $compiler ) {
	$compiler->addExtension( 'extensions', new Nette\DI\Extensions\ExtensionsExtension );
	$compiler->loadConfig( __dir__ . '/app/config/config.neon' );
	$compiler->loadConfig( __dir__ . '/app/config/config.local.neon' );
} );
$container = new $class;
/** @var \App\Container $appContainer */
$appContainer = $container->getByType( \App\Container::class );
$appContainer->startup();
