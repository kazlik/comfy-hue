<?php


namespace App\Model;


class Curl extends \Curl\Curl
{
	public function __construct( $base_url = null )
	{
		parent::__construct( $base_url );
		$this->setOpt( CURLOPT_SSL_VERIFYPEER, false );
	}


	public function get( $url, $data = [] )
	{
		$result = parent::get( $url, $data );
		if( $this->error ) {
			throw new \RuntimeException( 'Failed to fetch data from ' . $url );
		}
		return $result;
	}

}