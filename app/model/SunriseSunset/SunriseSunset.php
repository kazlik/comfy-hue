<?php


namespace App\Model\SunriseSunset;


use App\Model\Curl;

class SunriseSunset
{
	/** @var Config */
	private $config;

	/** @var SunriseSunsetEntity|null */
	private $cachedSunriseSunsetEntity;


	/**
	 * SunriseSunset constructor.
	 *
	 * @param Config $config
	 */
	public function __construct( Config $config )
	{
		$this->config = $config;
	}


	/**
	 * 0.0 - night
	 * 1.0 - day
	 *
	 * @return float
	 */
	public function getActualLightRatio(): float
	{
		if( $this->cachedSunriseSunsetEntity === null || !$this->cachedSunriseSunsetEntity->isCreatedToday() ) {
			$this->getActualSunriseSunset();
		}
		$timezone = new \DateTimeZone( "UTC" );
		$now = new \DateTime();
		$modifier = "60 minutes";
		$sunriseStart = new \DateTime( $this->cachedSunriseSunsetEntity->getSunrise(), $timezone );
		$sunriseEnd = new \DateTime( $this->cachedSunriseSunsetEntity->getSunrise(), $timezone );
		$sunsetStart = new \DateTime( $this->cachedSunriseSunsetEntity->getSunset(), $timezone );
		$sunsetEnd = new \DateTime( $this->cachedSunriseSunsetEntity->getSunset(), $timezone );
		$sunriseEnd->modify( sprintf( "+%s", $modifier ) );
		$sunsetStart->modify( sprintf( "-%s", $modifier ) );
		if( $now <= $sunriseStart || $now >= $sunsetEnd ) {
			return 0.0;
		} else if( $now >= $sunriseEnd && $now <= $sunsetStart ) {
			return 1.0;
		} else if( $now < $sunriseEnd ) {
			$sunriseLength = $this->getDiffInMinutesBetween( $sunriseStart, $sunriseEnd );
			$elapsedFromStart = $this->getDiffInMinutesBetween( $sunriseStart, $now );
			return $elapsedFromStart / $sunriseLength;
		} else if( $now > $sunsetStart ) {
			$sunsetLength = $this->getDiffInMinutesBetween( $sunsetStart, $sunsetEnd );
			$elapsedFromStart = $this->getDiffInMinutesBetween( $sunsetStart, $now );
			return 1 - ( $elapsedFromStart / $sunsetLength );
		} else {
			throw new \RuntimeException(
				sprintf( 'I came here and I don\'t know why (@%s)', $now->format( 'H:i' ) )
			);
		}
	}


	private function getActualSunriseSunset(): self
	{
		$curl = new Curl();
		$curl->get( sprintf( 'https://api.sunrise-sunset.org/json?lat=%f&lng=%f',
			$this->config->getLatitude(),
			$this->config->getLongitude() ) );
		$sunriseSunsetEntity = new SunriseSunsetEntity( $curl->response );
		$this->cachedSunriseSunsetEntity = $sunriseSunsetEntity;
		return $this;
	}


	private function getDiffInMinutesBetween( \DateTime $start, \DateTime $end ): int
	{
		$diff = $start->diff( $end );
		return $diff->h * 60 + $diff->i;
	}

}