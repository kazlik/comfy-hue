<?php


namespace App\Model\SunriseSunset;


class SunriseSunsetEntity
{

	/** @var \stdClass */
	private $data;

	/** @var string */
	private $createdAt;


	/**
	 * SunriseSunsetEntity constructor.
	 *
	 * @param \stdClass $data
	 */
	public function __construct( \stdClass $data )
	{
		$this->data = $data;
		$this->createdAt = date( 'Y-m-d' );
	}


	/**
	 * @return string
	 */
	public function getCreatedAt(): string
	{
		return $this->createdAt;
	}


	public function isCreatedToday(): bool
	{
		return $this->getCreatedAt() === date( 'Y-m-d' );
	}


	public function getSunrise(): string
	{
		return $this->data->results->sunrise;
	}


	public function getSunset(): string
	{
		return $this->data->results->sunset;
	}


	public function getSolarNoon(): string
	{
		return $this->data->results->solar_noon;
	}


	public function getDayLength(): string
	{
		return $this->data->results->day_length;
	}


	public function getCivilTwilightBegin(): string
	{
		return $this->data->results->civil_twilight_begin;
	}


	public function getCivilTwilightEnd(): string
	{
		return $this->data->results->civil_twilight_end;
	}


	public function getNauticalTwilightBegin(): string
	{
		return $this->data->results->nautical_twilight_begin;
	}


	public function getNauticalTwilightEnd(): string
	{
		return $this->data->results->nautical_twilight_end;
	}


	public function getAstronomicalTwilightBegin(): string
	{
		return $this->data->results->astronomical_twilight_begin;
	}


	public function getAstronomicalTwilightEnd(): string
	{
		return $this->data->results->astronomical_twilight_end;
	}


	public function getStatus(): string
	{
		return $this->data->status;
	}

}