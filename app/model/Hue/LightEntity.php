<?php


namespace App\Model\Hue;


class LightEntity
{

	private $data;


	/**
	 * LightEntity constructor.
	 *
	 * @param $data
	 */
	public function __construct( $data )
	{
		$this->data = $data;
	}


	public function isOn(): bool
	{
		return $this->data->state->on;
	}

}