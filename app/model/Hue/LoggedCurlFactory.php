<?php


namespace App\Model\Hue;


class LoggedCurlFactory
{

	/** @var Config */
	private $config;


	/**
	 * LoggedCurlFactory constructor.
	 *
	 * @param Config $config
	 */
	public function __construct( Config $config )
	{
		$this->config = $config;
	}


	public function create( $base_url = null ): LoggedCurl
	{
		return new LoggedCurl( $this->config, $base_url );
	}
}