<?php


namespace App\Model\Hue;


class Light
{
	/** @var LoggedCurlFactory */
	private $loggedCurlFactory;


	/**
	 * Light constructor.
	 *
	 * @param LoggedCurlFactory $loggedCurlFactory
	 */
	public function __construct( LoggedCurlFactory $loggedCurlFactory )
	{
		$this->loggedCurlFactory = $loggedCurlFactory;
	}


	/**
	 *
	 * @return LightEntity[]
	 */
	public function getAllLights(): array
	{
		$curl = $this->loggedCurlFactory->create();
		$curl->get( 'lights' );
		$array = [];
		foreach( $curl->response as $id => $data ) {
			$array[ $id ] = new LightEntity( $data );
		}
		return $array;
	}


	public function setLightState( int $id, array $settings )
	{
		$curl = $this->loggedCurlFactory->create();
		$curl->put( sprintf( 'lights/%d/state', $id ), json_encode( $settings ) );
	}
}