<?php


namespace App\Model\Hue\Bridge;


use App\Model\Curl;

class FetchUsername
{
	public function fetchForIp( $ip ): string
	{
		$curl = new Curl();
		$curl->post( 'http://' . $ip . '/api', json_encode( [ 'devicetype' => 'Comfy Hue' ] ) );
		if( isset( $curl->response[ 0 ]->error ) ) {
			if( $curl->response[ 0 ]->error->type === 101 ) {
				echo "Please press link button...\n";
				sleep( 2 );
				return $this->fetchForIp( $ip );
			} else {
				throw new \RuntimeException( 'Some error occured during username fetch' );
			}
		}
		return $curl->response[ 0 ]->success->username;
	}
}