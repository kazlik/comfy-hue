<?php


namespace App\Model\Hue\Bridge;



use App\Model\Curl;

class Discover
{
	public function discover()
	{
		$curl = new Curl();
		$curl->get( 'https://discovery.meethue.com/' );
		return $curl->response;
	}
}