<?php


namespace App\Model\Hue;


use App\Model\Curl;

class LoggedCurl extends Curl
{
	/** @var Config */
	private $config;


	public function __construct( Config $config, $base_url = null )
	{
		$this->config = $config;
		parent::__construct( $base_url );
	}


	public function get( $url, $data = [] )
	{
		return parent::get( $this->_getUrl( $url ), $data );
	}


	public function put( $url, $data = [] )
	{
		return parent::put( $this->_getUrl( $url ), $data );
	}


	private function _getUrl( string $url )
	{
		return sprintf( 'http://%s/api/%s/%s', $this->config->getIp(), $this->config->getUsername(), $url );
	}


}