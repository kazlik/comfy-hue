<?php


namespace App\Model\Hue;


class LightConfig
{

	/** @var int */
	private $id;

	/** @var bool */
	private $isOn = false;

	/** @var bool */
	private $isManualMode = false;


	/**
	 * LightConfig constructor.
	 *
	 * @param int $id
	 */
	public function __construct( int $id )
	{
		$this->id = $id;
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return bool
	 */
	public function isOn(): bool
	{
		return $this->isOn;
	}


	/**
	 * @param bool $isOn
	 * @return LightConfig
	 */
	public function setIsOn( bool $isOn ): LightConfig
	{
		$this->isOn = $isOn;
		return $this;
	}


	/**
	 * @return bool
	 */
	public function isManualMode(): bool
	{
		return $this->isManualMode;
	}


	/**
	 * @param bool $isManualMode
	 * @return LightConfig
	 */
	public function setIsManualMode( bool $isManualMode ): LightConfig
	{
		$this->isManualMode = $isManualMode;
		return $this;
	}


}