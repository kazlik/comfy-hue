<?php


namespace App\Model\Hue;


class Config
{
	/** @var string */
	private $id;
	/** @var string */
	private $ip;
	/** @var string */
	private $username;


	/**
	 * Config constructor.
	 *
	 * @param string $id
	 * @param string $ip
	 * @param string $username
	 */
	public function __construct( string $id, string $ip, string $username )
	{
		$this->id = $id;
		$this->ip = $ip;
		$this->username = $username;
	}


	/**
	 * @return string
	 */
	public function getId(): string
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getIp(): string
	{
		return $this->ip;
	}


	/**
	 * @return string
	 */
	public function getUsername(): string
	{
		return $this->username;
	}


}