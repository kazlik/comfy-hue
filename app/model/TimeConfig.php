<?php

namespace App\Model;

class TimeConfig
{
	/** @var string */
	private $timezone;


	/**
	 * @param string $timezone
	 */
	public function __construct( string $timezone )
	{
		$this->timezone = $timezone;
	}


	/**
	 * @return string
	 */
	public function getTimezone(): string
	{
		return $this->timezone;
	}


}