<?php


namespace App\Model;


use App\Model\Hue\Bridge\Discover;
use App\Model\Hue\Bridge\FetchUsername;

class InitializeConfig
{

	/** @var Discover */
	private $discover;

	/** @var FetchUsername */
	private $fetchUsername;


	/**
	 * InitializeConfig constructor.
	 *
	 * @param Discover      $discover
	 * @param FetchUsername $fetchUsername
	 */
	public function __construct( Discover $discover, FetchUsername $fetchUsername )
	{
		$this->discover = $discover;
		$this->fetchUsername = $fetchUsername;
	}


	public function initialize(): bool
	{
		$bridges = $this->discover->discover();
		$id = $bridges[ 0 ]->id;
		$ip = $bridges[ 0 ]->internalipaddress;
		$username = $this->fetchUsername->fetchForIp( $ip );
		$config = "parameters:\n" .
			sprintf( "\tid: %s\n", $id ) .
			sprintf( "\tip: %s\n", $ip ) .
			sprintf( "\tusername: %s\n", $username ) .
			"\tlatitude: 49.191\n" .
			"\tlongitude: 16.612\n" .
			"\ttimezone: Europe/Prague\n";
		return file_put_contents( __dir__ . '/../config/config.local.neon', $config ) !== false;
	}
}