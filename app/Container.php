<?php


namespace App;


use App\Model\Hue\Light;
use App\Model\Hue\LightConfig;
use App\Model\InitializeConfig;
use App\Model\SunriseSunset\SunriseSunset;
use App\Model\TimeConfig;

class Container
{

	/** @var InitializeConfig */
	private $initializeConfig;

	/** @var Light */
	private $light;

	/** @var SunriseSunset */
	private $sunriseSunset;

	/** @var TimeConfig */
	private $timeConfig;

	/** @var bool */
	private $isInitialized = false;

	/** @var LightConfig[] */
	private $lightConfigs = [];


	/**
	 * @param InitializeConfig $initializeConfig
	 * @param Light            $light
	 * @param SunriseSunset    $sunriseSunset
	 * @param TimeConfig       $timeConfig
	 */
	public function __construct( InitializeConfig $initializeConfig, Light $light, SunriseSunset $sunriseSunset, TimeConfig $timeConfig )
	{
		$this->initializeConfig = $initializeConfig;
		$this->light = $light;
		$this->sunriseSunset = $sunriseSunset;
		$this->timeConfig = $timeConfig;
	}


	public function startup(): void
	{
		if( !$this->isInitialized ) {
			if( strlen( file_get_contents( __dir__ . '/config/config.local.neon' ) ) === 0 ) {
				$this->initializeConfig->initialize();
				echo "Initialized local settings, please run script again\n";
				die();
			} else {
				echo "Using existing settings, woohoo\n";

			}
			$this->isInitialized = true;
		}
		date_default_timezone_set( $this->timeConfig->getTimezone() );
		$this->initLights();
		$this->lightApp();
	}


	private function lightApp()
	{
		try {
			while( true ) {
				$lightRatio = $this->sunriseSunset->getActualLightRatio();
				$warmestTemperature = 400;
				$coolestTemperature = 200;
				$relativeTemperature = (int) round( ( 1 - $lightRatio ) * ( $warmestTemperature - $coolestTemperature ) );
				$actualTemperature = $coolestTemperature + $relativeTemperature;
				echo sprintf( "%s Checking lights changes (temperature ratio %f)...\n",
					( new \DateTime() )->format( 'H:i:s' ),
					$lightRatio );
				$lights = $this->light->getAllLights();
				foreach( $lights as $id => $light ) {
					$lightConfig = $this->lightConfigs[ $id ];
					if( $lightConfig->isOn() !== $light->isOn() ) {
						$lightConfig->setIsOn( $light->isOn() );
						if( !$light->isOn() ) {
							$lightConfig->setIsManualMode( false );
						}
						echo sprintf( "Light %d just turned %s\n", $id, $lightConfig->isOn() ? 'ON' : 'OFF' );
					}
					if( $light->isOn() ) {
						$this->light->setLightState( $id, [ 'ct' => $actualTemperature ] );
					}
				}
				sleep( 5 );
			}
		} catch( \RuntimeException $e ) {
			sleep( 60 );
			$this->lightApp();
		}
	}


	private function initLights(): void
	{
		echo "Initializing lights...\n";
		$lights = $this->light->getAllLights();
		foreach( $lights as $id => $light ) {
			$lightConfig = new LightConfig( $id );
			$lightConfig->setIsOn( $light->isOn() );
			$this->lightConfigs[ $lightConfig->getId() ] = $lightConfig;
			echo sprintf( "Light %d is %s\n", $id, $lightConfig->isOn() ? 'ON' : 'OFF' );
		}
	}
}